using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class EnvironmentRenderer : MonoBehaviour
{
    [SerializeField] private float textureChangeTime;
    [SerializeField] private float fadeTime;
    [SerializeField] private List<Renderer> wallRenderers = new List<Renderer>();
    [SerializeField] private Material baseSkyboxMat;
    [SerializeField] private Texture2D baseWallTexture;

    public static Action onTransitionDone;

    Material baseSkybox;
    GameObject tempObj;

    private void OnEnable()
    {
        FactManager.OnLoadFact += loadFactEnvironment;
    }

    private void OnDisable()
    {
        FactManager.OnLoadFact -= loadFactEnvironment;
    }

    private void loadFactEnvironment(Fact _fact)
    {
        //clear previous environment
        if (!_fact.Method.WallTexture)
        {
            changeWallMat(baseWallTexture);
        }

        if (!_fact.Method.TransparentWall)
        {
            changeWallTransparent(1);
        }

        if (!_fact.Method.SkyBox) RenderSettings.skybox = baseSkybox;
        if (tempObj) tempObj.SetActive(false);
        //

        tempObj = _fact.FactObject;
        if (tempObj) tempObj.SetActive(true);

        if (_fact.Method.SkyBox) RenderSettings.skybox = _fact.Method.SkyBox;


        //Transition WallTexture
        if (_fact.Method.WallTexture)
        {
            changeWallMat(_fact.Method.WallTexture,true);
        }

        //Fade wall
        if (_fact.Method.TransparentWall)
        {
            changeWallTransparent(0,true);
        }
    }

    private void changeWallTransparent(float alpha, bool check = false)
    {
        foreach (Renderer i in wallRenderers)
        {
            float opacityValue = i.material.GetFloat("_Opacity");
            DOTween.To(() => opacityValue, x => opacityValue = x, alpha, fadeTime).OnUpdate(() => i.material.SetFloat("_Opacity", opacityValue)).OnComplete(() => { if (check) onTransitionDone?.Invoke(); });
        }
    }

    private void changeWallMat(Texture2D texture, bool check = false)
    {
        wallRenderers.ForEach(x => x.material.SetTexture("_SecondMat", texture));

        foreach (Renderer i in wallRenderers)
        {
            float transitionValue = i.material.GetFloat("_Transition");
            DOTween.To(() => transitionValue, x => transitionValue = x, 2f, textureChangeTime).OnUpdate(() => i.material.SetFloat("_Transition", transitionValue))
            .OnComplete(() =>
            { i.material.SetTexture("_MainMat", texture);  
              i.material.SetFloat("_Transition", 0);
              if(check) onTransitionDone?.Invoke();
            });
        }
    }
}

