using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteButton : MonoBehaviour
{
    public Action onButtonClick;
    private bool enable;
    private SpriteRenderer spriteRenderer => GetComponent<SpriteRenderer>();
    [SerializeField] AudioSource clickSound;

    private void OnMouseUp()
    {
        if (enable)
        {
            onButtonClick?.Invoke();
            clickSound.Play();
        }
    }

    public void disableButton()
    {
        spriteRenderer.DOColor(new Color(1,1,1,0.2f),0.3f);
        enable = false;
    }

    public void enableButton()
    {
        spriteRenderer.DOColor(new Color(1, 1, 1, 1f), 0.3f);
        enable = true;
    }
}
