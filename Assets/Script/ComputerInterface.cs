using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class ComputerInterface : MonoBehaviour
{
    [SerializeField] private SpriteButton exitButton;
    [SerializeField] private SpriteButton questionButton;
    [SerializeField] private SpriteButton shutDownButton;

    [SerializeField] private CanvasGroup endTransition;

    public static Action onQuestionButtonClick;

    private bool questDone = true;
    private bool textDone = true;

    private void OnEnable()
    {
        exitButton.onButtonClick += OnExitButtonClick;
        questionButton.onButtonClick += OnQuestionButtonClick;
        shutDownButton.onButtonClick += OnShutdownButtonClick;
        EnvironmentRenderer.onTransitionDone += checkTransitionDone;
        TextController.onTextDone += checkFactDone;
        FactManager.LoadFinal += enableShutDown;
    }

    private void OnDisable()
    {
        exitButton.onButtonClick -= OnExitButtonClick;
        questionButton.onButtonClick -= OnQuestionButtonClick;
        shutDownButton.onButtonClick -= OnShutdownButtonClick;
        EnvironmentRenderer.onTransitionDone -= checkTransitionDone;
        TextController.onTextDone -= checkFactDone;
        FactManager.LoadFinal -= enableShutDown;
    }

    private void Start()
    {
        exitButton.disableButton();
        questionButton.disableButton();
    }

    public void OnExitButtonClick()
    {
        GlobalInputController.Instance.playerAction.Enable();
        GlobalCameraController.Instance.CallCam("PlayerCam");
        Cursor.lockState = CursorLockMode.Locked;

        exitButton.disableButton();
        questionButton.disableButton();
    }

    public void OnQuestionButtonClick()
    {
        questDone = false;
        textDone = false;

        onQuestionButtonClick?.Invoke();

        exitButton.disableButton();
        questionButton.disableButton();
    }

    public void OnShutdownButtonClick()
    {
        print("F");

        shutDownButton.disableButton();
        exitButton.disableButton();

        endTransition.DOFade(1f, 2f).OnComplete(() => { SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex); }) ;
    }

    public void checkFactDone()
    {
        questDone = true;

        if (textDone)
        {
            exitButton.enableButton();
            questionButton.enableButton();
        }
    }

    public void checkTransitionDone()
    {
        textDone = true;

        if (questDone)
        {
            exitButton.enableButton();
            questionButton.enableButton();
        }
    }

    public void enableShutDown()
    {
        shutDownButton.gameObject.SetActive(true);
        questionButton.gameObject.SetActive(false);
        shutDownButton.enableButton();
    }
}
