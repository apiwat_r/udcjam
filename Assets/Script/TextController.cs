using RedBlueGames.Tools.TextTyper;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.ProBuilder;

public class TextController : MonoBehaviour
{
    [SerializeField] private TextMeshPro pcText;
    private FactManager factManager => GameObject.FindGameObjectWithTag("GameController").GetComponent<FactManager>();
    private TextTyper textTyper => GetComponent<TextTyper>();

    public static Action onTextDone;

    private void Awake()
    {
        textTyper.printCompleted.AddListener(textDone);
    }

    private void OnEnable()
    {
        FactManager.OnLoadFact += loadFactText;
        
    }

    private void OnDisable()
    {
        FactManager.OnLoadFact -= loadFactText;
    }

    public void loadFactText(Fact _fact)
    {
        pcText.text = "";
        textTyper.TypeText(_fact.FactMessage,0.05f);
    }

    public void textDone()
    {
        onTextDone?.Invoke();
    }
}
